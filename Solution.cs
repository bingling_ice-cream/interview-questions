﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHKMD_DOTNET_QUESTION
{
    public class Solution
    {
        private int MaxSumAfterPartitioning(int[] arr, int k)
        {
            if (ProgrammerCode == null)
            {
                throw new Exception("ProgrammerCode事件没有添加处理函数");
            }
            else
            {
                return ProgrammerCode.Invoke(arr, k);
            }
        }

        public delegate int MyDelegate(int[] arr, int k);
        public static event MyDelegate ProgrammerCode;

        private static List<string[]> datas = new List<string[]>()
        {
            Resource._1.Split('\n'),
            Resource._2.Split('\n'),
            Resource._3.Split('\n'),
            Resource._4.Split('\n'),
            Resource._5.Split('\n'),
            Resource._6.Split('\n'),
            Resource._7.Split('\n'),
            Resource._8.Split('\n'),
            Resource._9.Split('\n'),
            Resource._10.Split('\n'),
            Resource._11.Split('\n'),
            Resource._12.Split('\n'),
            Resource._13.Split('\n'),
            Resource._14.Split('\n'),
            Resource._15.Split('\n'),
            Resource._16.Split('\n'),
            Resource._17.Split('\n'),
            Resource._18.Split('\n'),
            Resource._19.Split('\n'),
            Resource._20.Split('\n'),
        };

        public void Go()
        {
            Dictionary<int, int> error = new Dictionary<int, int>();
            HashSet<int> correct = new HashSet<int>();
            Dictionary<int, Exception> exception = new Dictionary<int, Exception>();

            DateTime start = DateTime.Now;
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < datas.Count; i++)
            {
                int j = i;
                Task task = Task.Run(() =>
                {
                    int[] arr = JsonConvert.DeserializeObject<int[]>(datas[j][0]);
                    int k = Convert.ToInt32(datas[j][1]);
                    int result = Convert.ToInt32(datas[j][2]);

                    Solution solution = new Solution();
                    try
                    {
                        int cal = solution.MaxSumAfterPartitioning(arr, k);
                        if (cal == result)
                        {
                            lock (correct)
                            {
                                correct.Add(j + 1);
                            }
                        }
                        else
                        {
                            lock (error)
                            {
                                error.Add(j + 1, cal);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lock (exception)
                        {
                            exception.Add(j + 1, ex);
                        }
                    }
                });

                tasks.Add(task);
            }
            bool flag = Task.WaitAll(tasks.ToArray(), 1000 * tasks.Count);
            DateTime end = DateTime.Now;
            if (flag)
            {
                Console.WriteLine($"全过程[读取数据+算法运行]总共耗时:[{(end - start).TotalMilliseconds:0.000}]ms");
                Console.WriteLine($"总共测试数据:{correct.Count + error.Count + exception.Count}个");
                Console.WriteLine("");
                Console.WriteLine($"正确:{correct.Count}个");
                Console.WriteLine("");
                Console.WriteLine($"错误:{error.Count}个");
                foreach (int key in error.Keys)
                {
                    Console.WriteLine($"测试集[{key}]错误 : 计算结果[{error[key]}]与参考答案不符");
                }
                Console.WriteLine("");
                Console.WriteLine($"异常:{exception.Count}个");
                foreach (int key in exception.Keys)
                {
                    Console.WriteLine($"测试集[{key}]异常 : {exception[key].Message}");
                }
            }
            else
            {
                Console.WriteLine("算法超时");
            }
        }
    }
}
